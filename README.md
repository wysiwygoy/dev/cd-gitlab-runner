# Continuous Deployment #

Configurations for running Wysiwyg's continuous deployment pipeline with gitlab-runner.

## Registering the runner ##

    docker-compose run gitlab-runner register -n

## Running the runner ##

    docker-compose up

## CoreOS

gitlab-runner.service can be copied to /etc/systemd/system/

